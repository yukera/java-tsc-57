package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "remove all tasks";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CLEAR TASKS]");
        getTaskEndpoint().clearTasks(serviceLocator.getSession());
    }

}
