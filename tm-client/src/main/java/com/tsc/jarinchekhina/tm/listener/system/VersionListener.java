package com.tsc.jarinchekhina.tm.listener.system;

import com.jcabi.manifests.Manifests;
import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "system-version";
    }

    @NotNull
    @Override
    public String description() {
        return "display program version";
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));
    }

}
