package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectFinishByNameListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "finish project by name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectFinishByNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        getProjectEndpoint().finishProjectByName(serviceLocator.getSession(), name);
    }

}
