package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskBindByProjectIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "bind task to project by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskBindByProjectIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        getTaskEndpoint().bindTaskByProjectId(serviceLocator.getSession(), taskId, projectId);
    }


}
