package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import com.tsc.jarinchekhina.tm.endpoint.ProjectDTO;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectShowByNameListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-show-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "show project by name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectShowByNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = getProjectEndpoint().findProjectByName(serviceLocator.getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        print(project);
    }

}
