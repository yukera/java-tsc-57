package com.tsc.jarinchekhina.tm.listener.user;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractUserListener;
import com.tsc.jarinchekhina.tm.endpoint.UserEndpoint;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "change password";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@userChangePasswordListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        userEndpoint.setPassword(serviceLocator.getSession(), password);
    }

}
