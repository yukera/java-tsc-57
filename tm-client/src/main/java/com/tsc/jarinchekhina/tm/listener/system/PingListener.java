package com.tsc.jarinchekhina.tm.listener.system;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractUserListener;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class PingListener extends AbstractUserListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "system-ping";
    }

    @NotNull
    @Override
    public String description() {
        return "ping server";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@pingListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[PING]");
        if (sessionEndpoint.ping())
            System.out.println("Success");
        else
            System.out.println("No connect");
    }

}
