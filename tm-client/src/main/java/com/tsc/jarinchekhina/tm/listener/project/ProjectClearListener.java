package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "remove all projects";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CLEAR PROJECTS]");
        getProjectEndpoint().clearProjects(serviceLocator.getSession());
    }

}
