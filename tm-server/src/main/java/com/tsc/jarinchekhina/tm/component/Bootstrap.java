package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.endpoint.AbstractEndpoint;
import com.tsc.jarinchekhina.tm.enumerated.EntityActionType;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.service.PropertyService;
import com.tsc.jarinchekhina.tm.util.SystemUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    private static final MessageExecutor messageExecutor = new MessageExecutor();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    public static void sendMessage(
            @NotNull final Object object,
            @NotNull final EntityActionType actionType
    ) {
        messageExecutor.sendMessage(object, actionType);
    }

    @SneakyThrows
    public void init() {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        initUsers();
        initPID();
        initEndpoints();
    }

    public void initUsers() {
        if (getUserService().findByLogin("admin") == null)
            getUserService().create("admin", "admin", Role.ADMIN);
        if (getUserService().findByLogin("test") == null)
            getUserService().create("test", "test", "test@test.ru");
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager-server.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

}