package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyEmailException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyRoleException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.HashIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.model.User;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    public IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @Override
    @SneakyThrows
    public boolean checkRoles(@Nullable final String userId, @Nullable final Role... roles) {
        if (DataUtil.isEmpty(userId)) return false;
        if (roles == null) return true;
        @NotNull final User user = findById(userId);
        @NotNull final Role role = user.getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return true;
        }
        return false;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            return userRepository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final User user) {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull Collection<User> collection) {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.addAll(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void update(@NotNull final User user) {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        if (findByEmail(email) != null) throw new EmailRegisteredException(email);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setEmail(email);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setRole(role);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.add(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final User user) {
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = findById(userId);
            @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
            if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
            user.setPasswordHash(hashPassword);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public User update(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = findById(userId);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
