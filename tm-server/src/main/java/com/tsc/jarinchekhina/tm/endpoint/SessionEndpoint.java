package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ISessionEndpoint;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @WebMethod
    public boolean ping() {
        return true;
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.close(session);
    }

    @NotNull
    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.validate(session);
        return User.toDTO(sessionService.getUser(session));
    }

    @NotNull
    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        return sessionService.open(login, password);
    }

}
